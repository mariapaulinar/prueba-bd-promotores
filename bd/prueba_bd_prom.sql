-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 17, 2015 at 08:49 AM
-- Server version: 5.6.24
-- PHP Version: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `prueba_bd_prom`
--

-- --------------------------------------------------------

--
-- Table structure for table `grants`
--

CREATE TABLE IF NOT EXISTS `grants` (
  `id` int(11) NOT NULL COMMENT 'Identificador del permiso',
  `profile_id` int(11) NOT NULL COMMENT 'Id del perfil asociado',
  `route` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Indica la ruta a la que tiene permiso'
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `grants`
--

INSERT INTO `grants` (`id`, `profile_id`, `route`) VALUES
(1, 1, 'user/create'),
(2, 1, 'user/store'),
(3, 1, 'user/edit'),
(4, 1, 'user/update'),
(5, 1, 'user/destroy'),
(6, 2, 'user/create'),
(7, 2, 'user/store'),
(8, 3, 'user/edit'),
(9, 3, 'user/update'),
(10, 3, 'user/destroy'),
(11, 1, 'user/list'),
(12, 2, 'user/list'),
(13, 3, 'user/list'),
(14, 1, 'user/show'),
(15, 2, 'user/show'),
(16, 3, 'user/show');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
  `id` int(11) NOT NULL COMMENT 'Identificador del perfil',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Nombre interno del perfil',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Nombre para mostrar'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Tabla de perfiles';

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrador'),
(2, 'creator', 'Creador'),
(3, 'editor', 'Editor');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL COMMENT 'Identificador del usuario',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Correo electrónico',
  `password` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Contraseña encriptada',
  `first_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Nombre del usuario',
  `last_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Apellidos del usuario',
  `profile_id` int(11) NOT NULL COMMENT 'Id del perfil asociado. Id de la tabla profiles',
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Token para recordatorio'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Tabla de usuarios';

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `first_name`, `last_name`, `profile_id`, `remember_token`) VALUES
(1, 'maria.paulina.r.v@gmail.com', '', 'Administrador', 'Administrador', 1, 'nzb7B0bN2iHsvJKS5VR9SLoPCicebVRIYKPwG32VC25z2O7qpBbGh7DtiwDs');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `grants`
--
ALTER TABLE `grants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `grants`
--
ALTER TABLE `grants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador del permiso',AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador del perfil',AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador del usuario',AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
