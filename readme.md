## Prueba técnica

1. La base de datos se encuentra en el directorio bd, bajo el nombre de: prueba_bd_prom.sql

2. La base de datos inicialmente tiene un usuario administrador sin contraseña para realizar la prueba. El correo es: maria.paulina.r.v@gmail.com

3. Los perfiles creados son: Administrador, creador y editor; con los id 1, 2 y 3 respectivamente.

4. Cada perfil está asociado en una tabla llamada grants, donde se relaciona el id del perfil con la ruta. Estas rutas son las que se encuentran en el routes.php
