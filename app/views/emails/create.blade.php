<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Registro exitoso para {{ $first_name }} {{ $last_name }}</h2>

		<div>
                    <strong>Email:</strong> {{ $email }} <br />
                    <strong>Contraseña:</strong> {{ $password }}
		</div>
	</body>
</html>
