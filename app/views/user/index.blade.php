@extends('template.principal')

@section('title')
Listado de usuarios
@stop

@section('content')
<div class="row">
    <div class="col-md-4">
        <a href="{{ url('user/create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Nuevo usuario</a>
    </div>
</div>
<div class="col-md-12">
    <table class="table table-responsive">
        <thead>
            <tr>
                <th>Nombres</th>
                <th>Apellidos</th>
                <th>Correo electrónico</th>
                <th>Perfil</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr>
                <td>{{ $user->first_name }}</td>
                <td>{{ $user->last_name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ Util::getProfile($user->profile_id)->description }}</td>
                <td>
                    <a href="{{ url('user/show') }}?id={{ $user->id }}" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span> Ver</a>
                    <a href="{{ url('user/edit') }}?id={{ $user->id }}" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-pencil"></span> Editar</a>
                    <a href="{{ url('user/destroy') }}?id={{ $user->id }}" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> Borrar</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@stop