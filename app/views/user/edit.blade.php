@extends('template.principal')

@section('title')
Editar usuario
@stop

@section('content')
@if(Session::has('success'))
<div class="col-md-12">
    <div class="alert alert-success">
        Usuario actualizado satisfactoriamente.
    </div>
</div>
@else
@if(Session::has('error'))
<div class="col-md-12">
    <div class="alert alert-danger">
        Hubo un problema al guardar. Intente nuevamente.
    </div>
</div>
@endif
@endif
<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading"></div>
        <div class="panel-body">
            {{ Form::open(array('url' => 'user/update')) }}
                {{ Form::hidden('id', $user->id) }}
                <div class="form-group">
                    {{ Form::label('first_name', 'Nombres*') }}
                    {{ Form::text('first_name', $user->first_name, array('class' => 'form-control', 'placeholder' => 'Ingrese el nombre del usuario')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('last_name', 'Apellidos') }}
                    {{ Form::text('last_name', $user->last_name, array('class' => 'form-control', 'placeholder' => 'Ingrese el apellido del usuario')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('email', 'Correo electrónico*') }}
                    {{ Form::text('email', $user->email, array('class' => 'form-control', 'placeholder' => 'Ingrese el correo electrónico del usuario', 'readonly' => true)) }}
                </div>
                <div class="form-group">
                    {{ Form::label('profile_id', 'Perfil*') }}
                    {{ Form::select('profile_id', $profiles, $user->profile_id, array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    {{ Form::submit('Guardar usuario') }}
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop