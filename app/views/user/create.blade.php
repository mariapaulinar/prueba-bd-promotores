@extends('template.principal')

@section('title')
Crear usuario
@stop

@section('content')
@if(Session::has('success'))
<div class="col-md-12">
    <div class="alert alert-success">
        Usuario creado satisfactoriamente. Se ha enviado un correo electrónico con los datos de acceso.
    </div>
</div>
@else
@if(Session::has('error'))
<div class="col-md-12">
    <div class="alert alert-danger">
        Hubo un problema al guardar. Intente nuevamente.
    </div>
</div>
@endif
@endif
<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading"></div>
        <div class="panel-body">
            {{ Form::open(array('url' => 'user/store')) }}
                <div class="form-group">
                    {{ Form::label('first_name', 'Nombres*') }}
                    {{ Form::text('first_name', '', array('class' => 'form-control', 'placeholder' => 'Ingrese el nombre del usuario')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('last_name', 'Apellidos') }}
                    {{ Form::text('last_name', '', array('class' => 'form-control', 'placeholder' => 'Ingrese el apellido del usuario')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('email', 'Correo electrónico*') }}
                    {{ Form::text('email', '', array('class' => 'form-control', 'placeholder' => 'Ingrese el correo electrónico del usuario')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('profile_id', 'Perfil*') }}
                    {{ Form::select('profile_id', $profiles, '', array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    {{ Form::submit('Guardar nuevo usuario') }}
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop