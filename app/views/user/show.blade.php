@extends('template.principal')

@section('title')
Info del usuario
@stop

@section('content')
<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading"></div>
        <div class="panel-body">
                <div class="form-group">
                    {{ Form::label('first_name', 'Nombres') }}
                    {{ $user->first_name }}
                </div>
                <div class="form-group">
                    {{ Form::label('last_name', 'Apellidos') }}
                    {{ $user->last_name }}
                </div>
                <div class="form-group">
                    {{ Form::label('email', 'Correo electrónico') }}
                    {{ $user->email }}
                </div>
                <div class="form-group">
                    {{ Form::label('profile_id', 'Perfil') }}
                    {{ Util::getProfile($user->profile_id)->description }}
                </div>
        </div>
    </div>
</div>
@stop