@extends('template.principal')

@section('title')
Ingresar
@stop

@section('content')
@if(Session::has('error'))
<div class="col-md-12">
    <div class="alert alert-danger">
        Datos erróneos. Intente nuevamente.
    </div>
</div>
@endif
<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading"></div>
        <div class="panel-body">
            {{ Form::open(array('url' => 'authenticate')) }}
                <div class="form-group">
                    {{ Form::label('email', 'Correo electrónico*') }}
                    {{ Form::text('email', '', array('class' => 'form-control', 'placeholder' => 'Ingrese el correo electrónico del usuario')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('email', 'Contraseña*') }}
                    {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Ingrese la contraseña')) }}
                </div>
                <div class="form-group">
                    {{ Form::submit('Ingresar') }}
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop