<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('No autorizado', 401);
		}
		else
		{
			return Redirect::guest('login');
		}
	}
        
        //Cuando hay una sesión activa para el usuario, se comprueba que esté autorizado para acceder a la ruta actual.
        
        $current_route = Route::getCurrentRoute()->getPath(); //Se obtiene la ruta actual.
        
        $user_grants = Profile::find(Auth::user()->profile_id)->grants; //Se obtienen los permisos asociados al perfil del usuario.
        
        $trusted = false; //Inicialmente esta bandera indica que no está permitido.
            
        foreach($user_grants as $user_grant) { //Se recorren lo permisos asociados.
            if($user_grant->route == $current_route) { //Si la ruta asociada al permiso es igual a la actual
                $trusted = true; //Se activa la bandera para permitir el acceso a esta ruta.
                break;
            }
        }
        
        if(!$trusted) { //Si la bandera se mantuvo en no permitido, no permita el acceso.
            return Response::make('No está autorizado para realizar esta acción. <a href="' . url('user/list') . '">Regresar</a>', 401);
        }
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});
