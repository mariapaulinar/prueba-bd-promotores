<?php

class UserController extends \BaseController {

	/**
	 * Devuelve el listado de usuarios creados.
	 *
	 * @return Response
         * @author María Ramírez <maria.paulina.r.v@gmail.com>
	 */
	public function index()
	{
            $users = User::all();
            return View::make('user.index')->with('users', $users);
	}
        

	/**
	 * Muestra el formulario para crear un nuevo usuario.
	 *
	 * @return Response
         * @author María Ramírez <maria.paulina.r.v@gmail.com>
	 */
	public function create()
	{
            $profiles = Profile::lists('description', 'id');
            return View::make('user.create')->with('profiles', $profiles);
	}


	/**
	 * Guarda el nuevo usuario.
	 *
	 * @return Response
         * @author María Ramírez <maria.paulina.r.v@gmail.com>
	 */
	public function store()
	{
            //Validar los datos
            $rules = array(
                'first_name' => 'required',
                'email' => 'required|email|unique:users,email',
            );
            
            $messages = array(
                'required' => 'Este campo es obligatorio.',
                'email'  => 'Correo electrónico inválido.',
                'unique'  => 'Este correo ya se encuentra registrado.'
            );
            $validator = Validator::make(Input::all(), $rules, $messages);
            
            if($validator->fails()) { //Si no es válido regresar al form con los errores.
                return Redirect::back()
                        ->withInput()
                        ->withErrors($validator);
            } else {
                //Se guarda el nuevo usuario.
                $email = Input::get('email');
                
                $user = new User();
                
                $user->first_name = Input::get('first_name');
                $user->last_name = Input::get('last_name');
                $user->email = $email;
                $password = time();
                $user->password = Hash::make($user->email);
                $user->profile_id = Input::get('profile_id');
                
                
                if($user->save()) { //Si todo va bien, se regresa al formulario con mensaje, correo y contraseña asignados.
                    $datos_email = array(
                        'first_name' => $user->first_name,
                        'last_name' => $user->last_name,
                        'email' => $user->email,
                        'password' => $password,
                    );
                    
                    $nombres = $user->first_name . ' ' . $user->last_name;
                    Mail::send('emails.create', $datos_email, function($message) use ($email, $nombres) {
                        $message->to($email, $nombres)->subject('Registro exitoso');
                    });
                    return Redirect::back()->with(array(
                        'success' => true,
                    ));
                } else { //Si no guarda, se devuelve con error.
                    return Redirect::back()->with(array(
                        'error' => true,
                    ));
                }
            }
	}


	/**
	 * Muestra el detalle del usuario.
	 *
	 * @return Response
         * @author María Ramírez <maria.paulina.r.v@gmail.com>
	 */
	public function show()
	{
            $user = User::find(Input::get('id'));
            return View::make('user.show')->with('user', $user);
	}


	/**
	 * Muestra el formulario para editar un usuario.
	 *
	 * @return Response
         * @author María Ramírez <maria.paulina.r.v@gmail.com>
	 */
	public function edit()
	{
            $user = User::find(Input::get('id'));
            $profiles = Profile::lists('description', 'id');
            return View::make('user.edit')->with(array('user' => $user, 'profiles' => $profiles));
	}


	/**
	 * Actualiza los datos del usuario.
	 *
	 * @return Response
         * @author María Ramírez <maria.paulina.r.v@gmail.com>
	 */
	public function update()
	{
            $user_id = Input::get('id');
            //Validar los datos
            $rules = array(
                'first_name' => 'required',
            );
            
            $messages = array(
                'required' => 'Este campo es obligatorio.',
            );
            $validator = Validator::make(Input::all(), $rules, $messages);
            
            if($validator->fails()) { //Si no es válido regresar al form con los errores.
                return Redirect::back()
                        ->withInput()
                        ->withErrors($validator);
            } else {
                //Se guarda el usuario.
                $user = User::find($user_id);
                
                $user->first_name = Input::get('first_name');
                $user->last_name = Input::get('last_name');
                $user->profile_id = Input::get('profile_id');
                if($user->save()) { //Si todo va bien, se regresa al formulario con mensaje.
                    return Redirect::back()->with(array(
                        'success' => true,
                    ));
                } else { //Si no guarda, se devuelve con error.
                    return Redirect::back()->with(array(
                        'error' => true,
                    ));
                }
            }
	}


	/**
	 * Elimina un usuario.
	 *
	 * @return Response
         * @author María Ramírez <maria.paulina.r.v@gmail.com>
	 */
	public function destroy()
	{
            $user = User::find(Input::get('id'));
            $user->delete();
            
            return Redirect::back();
	}
        
        
        /**
	 * Muestra el formulario de login.
	 *
	 * @return Response
         * @author María Ramírez <maria.paulina.r.v@gmail.com>
	 */
	public function login()
	{
            return View::make('user.login');
	}
        
        
        /**
	 * Autentica el usuario.
	 *
	 * @return Response
         * @author María Ramírez <maria.paulina.r.v@gmail.com>
	 */
	public function authenticate()
	{
            $email = Input::get('email');
            $password = Input::get('password');
            
            //Si es el usuario inicial de prueba, redirigir.
            if($email == 'maria.paulina.r.v@gmail.com') {
                $user = User::find(1);
                Auth::login($user);
                return Redirect::intended('user/list');
            }
            
            //Verificar los datos de acceso.
            if (Auth::attempt(array('email' => $email, 'password' => $password))) {
                return Redirect::intended('user/list');
            } else {
                return Redirect::back()->with('error', true);
            }
	}
        
        
        /**
	 * Cierra la sesión del usuario.
	 *
	 * @return Response
         * @author María Ramírez <maria.paulina.r.v@gmail.com>
	 */
	public function logout()
	{
            Auth::logout();
            return Redirect::to('login');
	}


}
