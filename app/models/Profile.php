<?php

class Profile extends Eloquent {

	/**
	 * Tabla profiles.
	 *
	 * @var string
	 */
	protected $table = 'profiles';
        
        public $timestamps = false;
        
        public function grants()
        {
            return $this->hasMany('Grant');
        }

}
