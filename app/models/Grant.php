<?php

class Grant extends Eloquent {

	/**
	 * Tabla grants.
	 *
	 * @var string
	 */
	protected $table = 'grants';
        
        public $timestamps = false;

}
