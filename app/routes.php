<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('prueba-plantilla', function()
{
    return View::make('template.principal');
});

Route::get('/', function() {
    return Redirect::to('user/list');
}); //Listado de usuarios
Route::get('login', 'UserController@login'); //Login
Route::post('authenticate', 'UserController@authenticate'); //Autenticar
Route::get('logout', 'UserController@logout'); //Cerrar sesión

Route::group(array('prefix' => 'user', 'before' => 'auth'), function() {
    Route::get('/', function() {
        return Redirect::to('user/list');
    }); //Listado de usuarios
    Route::get('create', 'UserController@create'); //Crear
    Route::post('store', 'UserController@store'); //Guardar nuevo usuario
    Route::get('list', 'UserController@index'); //Listado de usuarios
    Route::get('show', 'UserController@show'); //Ver
    Route::get('edit', 'UserController@edit'); //Editar
    Route::post('update', 'UserController@update'); //Guardar usuario editado
    Route::get('destroy', 'UserController@destroy'); //Eliminar un usuario
});

