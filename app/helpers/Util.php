<?php

/**
 * Clase para funciones generales
 *
 * @author maria
 */
class Util {
    
    /**
    * Devuelve modelo para el perfil dado.
    *
    * @return Model
    */
    public static function getProfile($profile_id)
    {
        $profile = Profile::find($profile_id);
        
        return $profile;
    }
}
